FROM python:3.12.3-slim
EXPOSE $WEBSITE_PORT $DB_PORT
RUN apt-get update && apt-get -y install apt-utils vim curl firefox-esr && apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y
RUN pip install --upgrade pip
ENV HOME=/var/www/ \
    WebsiteHOME=/var/www/website/ \
    DriversHOME=/var/www/drivers/ \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1
RUN env
RUN mkdir -p $WebsiteHOME && mkdir -p $DriversHOME

RUN useradd -m event_manager

WORKDIR $DriversHOME
COPY drivers/ ./
RUN chmod +x gecko* && export PATH=$PATH:$DriversHOME

WORKDIR $HOME
COPY requirements.txt .env ./
RUN pip install -r requirements.txt

COPY src/init/init_app.sh src/init/jobs_app.sh ./

WORKDIR $WebsiteHOME
COPY src/website ./

WORKDIR $HOME
RUN chown -R event_manager:event_manager $HOME && chmod -R 755 $HOME && chmod -R 777 /var/log

USER event_manager
RUN echo 'alias ll="ls -l"' >> ~/.bashrc
RUN export PATH=$PATH:$HOME