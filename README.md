# EventManager

## Prepare environment

- [ ] Clone this repository to <your_path>

- [ ] Create data directory in `mkdir-p ./data`

- [ ] Fill in specific information like passwords and paths inside the ``.env`` file:
  ```.env
  DB_USER=<DB_USER>
  DB_PASSWORD=<DB_PASSWORD>
  DB_HOST=<DB_HOST>
  DB_PORT=<DB_PORT>
  WEBSITE_PORT=<WEBSITE_PORT>
  ```

## Run the application

To run the application you need to run docker compose command:

```bash
cd <your_path>
docker-compose -f docker-compose.yml up --build -d
```
