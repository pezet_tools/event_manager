#!/bin/bash

param="${1}"

update(){
  _param="${1}"
  if [ "${_param}" == "0220" ] || [ "${_param}" == "MP" ]; then
    curl --location 'localhost:8000/event_manager/scrapper/mp' >> /var/log/tastings_mp.log 2>&1 &
  fi
  if [ "${_param}" == "0230" ] || [ "${_param}" == "WTP" ]; then
    curl --location 'localhost:8000/event_manager/scrapper/wtp' >> /var/log/commute_wtp.log 2>&1 &
  fi
  if [ "${_param}" == "0300" ] || [ "${_param}" == "DEL" ]; then
    curl --location 'localhost:8000/event_manager/delete_old_events' >> /var/log/events.log 2>&1 &
  fi
}

run_jobs(){
  if [ "${1}" == "" ]; then
    # shellcheck disable=SC2050
    while [ 1 == 1 ]; do
      _date=$(date "+%H%M")
      update "${_date}"
      sleep 60
    done
  else
    update "${1}"
  fi
}

run_jobs "${param}"
