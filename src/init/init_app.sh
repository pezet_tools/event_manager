#!/bin/bash

# JOBS CONFIG
./jobs_app.sh >> /var/log/jobs.log 2>&1 &

# Server start
uvicorn website.main:app --host 0.0.0.0 --port 8000 --reload
