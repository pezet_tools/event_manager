# Settings and global variables
from pathlib import Path
from dotenv import dotenv_values

# Read environment variables
environment_variables = dotenv_values()
DB_HOST = environment_variables.get('DB_HOST')
DB_PORT = environment_variables.get('DB_PORT')
DB_USER = environment_variables.get('DB_USER')
DB_PASSWORD = environment_variables.get('DB_PASSWORD')

drivers_dir = Path(__file__).parent.parent.joinpath("drivers")
FIREFOX_DRIVER = f"{drivers_dir}/geckodriver"
FIREFOX_BINARY = "/usr/bin/firefox"
