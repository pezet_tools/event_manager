from datetime import date, datetime, timedelta
from .scrapper import Scrapper
from ..lib.models import EventEnums


class WTPScrapper(Scrapper):

    def __init__(self, url: str):
        super().__init__(url)

    def get_webpage_tags(self, feeds: dict):
        entries: list[dict] = feeds.get('entries')
        for entry in entries:
            _dict = dict()
            _dict[EventEnums.NAME] = entry.get('title')
            _dict[EventEnums.DESCRIPTION] = entry.get('summary')
            _dict[EventEnums.DATE_START] = entry.get('published')
            _dict[EventEnums.DATE_END] = None
            _dict[EventEnums.LOCATION] = "Warszawa"
            _dict[EventEnums.PRICE] = 0
            _dict[EventEnums.TICKETS_LEFT] = 0
            _dict[EventEnums.DATETIME_UNTIL] = None
            _dict[EventEnums.URL] = entry.get('link')

            self.content_tag_list.append(_dict)

    def _parse_name(self, value: str) -> str:
        return super()._parse_name(value)

    def _parse_description(self, value: str) -> str:
        return super()._parse_description(value)

    def _parse_location(self, value: str) -> str:
        return super()._parse_location(value)

    def _parse_date_start(self, value: str, _format: str = "%a, %d %b %Y %H:%M:%S %z") -> datetime.date:
        return super()._parse_date_start(value, _format)

    def _parse_date_end(self, value: str, _format: str = None) -> datetime.date:
        return super()._parse_date_end(value)

    def _parse_time_start(self, value: str, _format: str = None) -> str:
        return super()._parse_time_start(value)

    def _parse_time_end(self, value: str, _format: str = None) -> str:
        return super()._parse_time_end(value)

    def _parse_price(self, value: str | int) -> float:
        return super()._parse_price(value)

    def _parse_tickets_left(self, value: str | int) -> int:
        return super()._parse_tickets_left(value)

    def _parse_datetime_until(self, value: str, _format: str = None) -> datetime | None:
        return super()._parse_datetime_until(value)

    def _parse_event_url(self, value: str) -> str:
        return super()._parse_event_url(value)

    def scrap_website(self) -> list[dict]:
        """
        Main scrapping method
        :return: List of events as dictionary entries
        """
        feeds: dict = self.get_feeds()
        self.get_webpage_tags(feeds)
        self.parse_webpage_contents()
        for content_element in self.content_list:
            print(f"{content_element=}")
        return self.content_list
