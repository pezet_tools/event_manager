from datetime import date, datetime, timedelta
from .scrapper import Scrapper
from ..lib.models import EventEnums


class MPScrapper(Scrapper):

    def __init__(self, url: str):
        super().__init__(url)

    def get_webpage_tags(self, main_page):

        ticket_groups = main_page.findAll('div', class_='l-tickets')

        for group in ticket_groups:
            articles = group.findAll('article', class_="c-ticket")

            for article in articles:
                _dict = dict()
                _dict[EventEnums.NAME] = article.find('h2', class_="c-ticket__name")
                _dict[EventEnums.DESCRIPTION] = article.find('p', class_="c-ticket__desc")
                _info = article.find('div', class_="c-ticket__info").findAll('p', class_="")
                _datetime = _info[0].findAll('span', class_="h-ws-nw")
                _dict[EventEnums.DATE_START] = _datetime[0]
                _dict[EventEnums.DATE_END] = _datetime[0]
                _dict[EventEnums.TIME_START] = _datetime[1] if len(_datetime) > 1 else None
                _dict[EventEnums.TIME_END] = _datetime[1] if len(_datetime) > 1 else None
                _dict[EventEnums.LOCATION] = _info[1]
                _dict[EventEnums.PRICE] = article.find('span', class_='c-cart-quantity__price')
                _dict[EventEnums.TICKETS_LEFT] = article.find('div', class_='c-ticket__left')
                _time_left = article.find('div', class_='c-ticket__countdown')
                _dict[EventEnums.DATETIME_UNTIL] = _time_left.span if _time_left else None
                _dict[EventEnums.URL] = None

                self.content_tag_list.append(_dict)

    def _parse_name(self, value: str) -> str:
        return value

    def _parse_description(self, value: str) -> str:
        if value:
            return value

    def _parse_location(self, value: str) -> str:
        if value:
            return value.replace('Miejsce:', '')

    def _parse_date_start(self, value: str, _format: str = None) -> date:
        if value:
            dates = value.split('-')
            _date_string = dates[0]
            if _date_string:
                _day, _month, _year = _date_string.strip(' ').split('.')
                _date = date(day=int(_day), month=int(_month), year=int(_year))
                return _date

    def _parse_date_end(self, value: str, _format: str = None) -> date:
        if value:
            dates = value.split('-')
            _date_string = dates[1] if len(dates) > 1 else None
            if _date_string:
                _day, _month, _year = _date_string.strip(' ').split('.')
                _date = date(day=int(_day), month=int(_month), year=int(_year))
                return _date

    def _parse_time_start(self, value: str, _format: str = None) -> str:
        if value:
            time_array = value.split('-')
            _time = time_array[0].strip(' ')
            if _time:
                return _time

    def _parse_time_end(self, value: str, _format: str = None) -> str:
        if value:
            time_array = value.split('-')
            _time = time_array[1].strip(' ') if len(time_array) > 1 else None
            if _time:
                return _time

    def _parse_price(self, value: str) -> float:
        if value:
            return float(value.replace(' zł', ''))

    def _parse_tickets_left(self, value: str) -> int:
        if value:
            return int(value.replace('Zostało biletów: ', '').replace(' szt.', ''))

    def _parse_datetime_until(self, value: str, _format: str = None) -> datetime:
        if value:
            temp_value = value.replace('Sprzedaż biletów kończy się za: ', '')
            _days, _time = temp_value.split(' dni ')
            _hours, _minutes, _seconds = _time.split(':')
            _datetime_until = datetime.now() + timedelta(days=int(_days),
                                                         hours=int(_hours),
                                                         minutes=int(_minutes),
                                                         seconds=int(_seconds))
            return _datetime_until

    def _parse_event_url(self, value: str) -> str | None:
        return super()._parse_event_url(value)

    def scrap_website(self) -> list[dict]:
        """
        Main scrapping method
        :return: List of events as dictionary entries
        """
        webpage_content = MPScrapper.get_webpage(self.url)
        self.get_webpage_tags(webpage_content)
        self.parse_webpage_contents()
        for content_element in self.content_list:
            print(f"{content_element=}")
        return self.content_list
