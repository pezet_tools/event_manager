import re
import datetime
import httpx
import feedparser
from typing import Final
from bs4 import BeautifulSoup, Tag
from selenium import webdriver
from abc import ABC, abstractmethod
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options

from ..lib.models import EventEnums
from ..settings import FIREFOX_DRIVER, FIREFOX_BINARY


class Scrapper(ABC):

    HEADERS: Final = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0'
    }
    HTML_PARSER: Final = 'html.parser'

    def __init__(self, url: str):
        self.url = url
        self.content_tag_list: list[dict[str, Tag]] = list()
        self.content_list: list[dict[str, str]] = list()
        self.event_type = 0

    @staticmethod
    def get_webpage(url: str) -> BeautifulSoup:
        service = Service(executable_path=FIREFOX_DRIVER)
        options = Options()
        options.binary_location = FIREFOX_BINARY
        options.add_argument('-headless')

        with webdriver.Firefox(service=service, options=options) as browser:
            browser.get(url)
            main_page = BeautifulSoup(browser.page_source, Scrapper.HTML_PARSER)
            return main_page

    def get_feeds(self) -> dict:
        resp = httpx.get(self.url, headers=Scrapper.HEADERS)
        return feedparser.parse(resp.text)

    def parse_webpage_contents(self):
        for content in self.content_tag_list:
            _dict = dict()
            for field_name, field_value in content.items():
                _dict[field_name] = self.parse_value(field_name, field_value)
            self.content_list.append(_dict)

    @staticmethod
    def _default_parse(value: str | Tag) -> str:
        """
        Default parsing for each website element (new lines, hard spaces, multiple spaces, etc)
        :return:
        """
        if value is not None:
            if isinstance(value, Tag):
                new_value = value.text
            elif isinstance(value, str):
                new_value = value
            else:
                return value
            new_value = new_value.replace("\n", '').replace(u'\xa0', u' ')
            new_value = re.sub(' +', ' ', new_value)
            return new_value.strip(' ')

    def parse_value(self, field_type: str, field_value: Tag | str):
        _value = Scrapper._default_parse(field_value)
        match field_type:
            case EventEnums.NAME:
                return self._parse_name(_value)
            case EventEnums.DESCRIPTION:
                return self._parse_description(_value)
            case EventEnums.LOCATION:
                return self._parse_location(_value)
            case EventEnums.DATE_START:
                return self._parse_date_start(_value)
            case EventEnums.DATE_END:
                return self._parse_date_end(_value)
            case EventEnums.TIME_START:
                return self._parse_time_start(_value)
            case EventEnums.TIME_END:
                return self._parse_time_end(_value)
            case EventEnums.PRICE:
                return self._parse_price(_value)
            case EventEnums.TICKETS_LEFT:
                return self._parse_tickets_left(_value)
            case EventEnums.DATETIME_UNTIL:
                return self._parse_datetime_until(_value)
            case EventEnums.URL:
                return self._parse_event_url(_value)

    @abstractmethod
    def _parse_name(self, value: str) -> str | None:
        """
        Parsing event name from a website
        :return:
        """
        return value

    @abstractmethod
    def _parse_description(self, value: str) -> str | None:
        """
        Parsing event description from a website
        :return:
        """
        return value

    @abstractmethod
    def _parse_location(self, value: str) -> str | None:
        """
        Parsing event location from a website
        :return:
        """
        return value

    @abstractmethod
    def _parse_date_start(self, value: str, _format: str = None) -> datetime.date | None:
        """
        Parsing event date from a website
        :return:
        """
        if _format:
            return datetime.datetime.strptime(value, _format)
        return None

    @abstractmethod
    def _parse_date_end(self, value: str, _format: str = None) -> datetime.date | None:
        """
        Parsing event date from a website
        :return:
        """
        if _format:
            return datetime.datetime.strptime(value, _format)
        return None

    @abstractmethod
    def _parse_time_start(self, value: str, _format: str = None) -> str | None:
        """
        Parsing event time from a website
        :return:
        """
        if _format:
            return str(datetime.datetime.strptime(value, _format))
        return None

    @abstractmethod
    def _parse_time_end(self, value: str, _format: str = None) -> str | None:
        """
        Parsing event time from a website
        :return:
        """
        if _format:
            return str(datetime.datetime.strptime(value, _format))
        return None

    @abstractmethod
    def _parse_price(self, value: str | int) -> float | None:
        """
        Parsing price for the event from a website
        :return:
        """
        return float(value)

    @abstractmethod
    def _parse_tickets_left(self, value: str | int) -> int | None:
        """
        Parsing tickets left for the event from a website
        :return:
        """
        return int(value)

    @abstractmethod
    def _parse_datetime_until(self, value: str, _format: str = None) -> datetime.datetime | None:
        """
        Parsing time left for buying tickets from a website
        :return:
        """
        if _format:
            return datetime.datetime.strptime(value, _format)
        return None

    @abstractmethod
    def _parse_event_url(self, value: str) -> str | None:
        """
        Parsing direct url to the event
        :return:
        """
        return value

    @abstractmethod
    def scrap_website(self) -> list[dict]:
        """
        Main scrapping method
        :return: List of events as dictionary entries
        """
        ...
