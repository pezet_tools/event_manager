from nicegui import ui
from ..lib.session import MySession
from ..lib.models import EventType
from . import home, tastings, rockets, wtp


def create_banner():
    ui.image('/static/img/events_banner.jpg').props('fit=scale-down')


def create_tabs() -> None:
    session = MySession.get_session()
    categories = session.query(EventType).all()
    session.close()
    if not categories:
        return

    # create_banner()
    with ui.row().style("width: 100%"):

        with ui.tabs() as tabs:
            with ui.column():
                create_banner()
                ui.tab("Strona główna")
                ui.tab("Degustacje")
                ui.tab("Starty rakiet")
                ui.tab("Komunikacja miejska")

        with ui.tab_panels(tabs, value='Strona główna').style("width: 85%"):
            with ui.tab_panel('Strona główna').style("width: 100%"):
                home.page()
            with ui.tab_panel('Degustacje').style("width: 100%"):
                tastings.page()
            with ui.tab_panel('Starty rakiet').style("width: 100%"):
                rockets.page()
            with ui.tab_panel('Komunikacja miejska').style("width: 100%"):
                wtp.page()


def main_page() -> None:
    create_tabs()
