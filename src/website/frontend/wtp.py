from nicegui import ui
from functools import partial
from ..lib.models import Event, EventType, Website
from ..lib.session import MySession


def page() -> None:
    session = MySession.get_session()
    results = session.query(Event) \
        .join(EventType) \
        .filter_by(name='Komunikacja miejska') \
        .join(Website) \
        .order_by(Event.is_interested.desc(), Event.date_start, Event.time_start).all()
    session.commit()

    def open_dialog_window(element):
        dialog_content.set_content("")
        content = ""
        content += f"### {element.name}\n\n"
        content += f"{element.description}\n\n"
        content += f"#### Data: {element.date_start}" + (f" - {element.date_end}" if element.date_end else "") + "\n\n"
        content += f"#### Czas: {element.time_start}" + (f" - {element.time_end}" if element.time_end else "") + "\n\n"
        content += f"#### Miejsce: {element.location}\n\n"
        content += f"Bilety: {element.tickets_left if element.tickets_left else 0}\n\n"
        content += f"Cena: {element.price}\n\n"
        content += f"Sprzedaż do: {element.datetime_until}\n\n"
        dialog_content.set_content(content)
        dialog_window.open()

    def set_as_interesting(status_element, event):
        event.is_interested = True
        session.commit()
        return_status(status_element, event)

    def set_as_not_interesting(status_element, event):
        event.is_interested = False
        session.commit()
        return_status(status_element, event)

    def return_status(status_element, event):
        status_element.clear()
        with status_element:
            if event.is_interested:
                ui.button("").props('icon=check color=green').classes('text-1sm').style("margin-right: 5px")
            elif event.is_interested is None:
                ui.button("").props('icon=question_mark color=blue').classes('text-1sm').style("margin-right: 5px")
            else:
                ui.button("").props('icon=delete_forever color=red').classes('text-1sm').style("margin-right: 5px")

    def show_all_buttons(status_element, url):
        ui.button("").props('outline icon=visibility color=primary').classes('text-1sm') \
            .style("margin-right: 5px") \
            .on("click", partial(open_dialog_window, result))
        ui.button("", on_click=lambda: ui.open(url)) \
            .props('outline icon=launch color=primary').classes('text-1sm').style("margin-right: 5px")
        ui.button("").props('outline icon=check color=green').classes('text-1sm').style("margin-right: 5px") \
            .on("click", partial(set_as_interesting, status_element, result))
        ui.button("").props('outline icon=delete_forever color=red').classes('text-1sm') \
            .on("click", partial(set_as_not_interesting, status_element, result))

    def close_dialog_window():
        dialog_content.set_content("")
        dialog_window.close()

    ui.markdown("### Informacje o Warszawskim Transporcie Publicznym")
    ui.markdown("[Aktualizuj](/event_manager/scrapper/wtp)")
    ui.element('br')
    ui.element('br')

    if results:
        for result in results:
            with ui.card().style('width: 95%; margin-bottom: 20px; margin-right: 20px; margin-left: 20px'):
                with ui.element("div").style("width: 100%; text-align: right"):
                    ui.label(result.website.name).style('color: #996600')
                ui.label(result.name).style('font-weight: bold')
                ui.markdown(f"{result.description}")
                ui.markdown(f"Data publikacji: {result.date_start}")
                with ui.element("div").style("width: 100%; text-align: right"):
                    with ui.dialog() as dialog_window, ui.card():
                        with ui.column():
                            dialog_content = ui.markdown(result.description)
                            ui.button('Zamknij', on_click=close_dialog_window)
                    status_box = ui.element("div").style("width: 100%; text-align: left")
                    return_status(status_box, result)
                    show_all_buttons(status_box, result.url)
