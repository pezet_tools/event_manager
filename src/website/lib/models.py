from sqlalchemy import Column, Integer, Float, String, Boolean, Date, DateTime, ForeignKey, UniqueConstraint
from sqlalchemy.orm import declarative_base, relationship
from typing import Final
from ..lib.session import MySession


Base = declarative_base()


class EventType(Base):
    __tablename__ = "event_type"

    id = Column(Integer, primary_key=True, nullable=False, comment="Unique event type id")
    name = Column(String, nullable=False, unique=True, comment="Unique name of the event type")
    description = Column(String, comment="Description of the event type")


class Website(Base):
    __tablename__ = "website"

    id = Column(Integer, primary_key=True, nullable=False, comment="Unique website id")
    name = Column(String, nullable=False, unique=True, comment="Unique name of the website")
    url = Column(String, nullable=False, unique=True, comment="Unique website url for scrapping")
    description = Column(String, comment="Description of the website")


class Event(Base):
    __tablename__ = "event"

    id = Column(Integer, primary_key=True, nullable=False, comment="Unique event id")
    name = Column(String, nullable=False, comment="Unique name of the event")
    description = Column(String, comment="Description of the event")
    location = Column(String, comment="Location when the event takes place")
    date_start = Column(Date, comment="Date when the event starts")
    date_end = Column(Date, comment="Date when the event ends (often the same as date_start, "
                                    "meaning event takes place only on one day)")
    time_start = Column(String, comment="Time when the event starts "
                                        "(sometimes time of the event is put inside description field)")
    time_end = Column(String, comment="Time when the event ends")
    price = Column(Float, comment="Price of entering the event / ticket")
    tickets_left = Column(Integer, comment="How many tickets have left for sell")
    datetime_until = Column(DateTime, comment="Time until buying is possible")
    url = Column(String, comment="Web address to this event", nullable=True)
    event_type_id = Column(Integer, ForeignKey("event_type.id"), nullable=False)
    website_id = Column(Integer, ForeignKey("website.id"), nullable=False)
    is_interested = Column(Boolean, comment="User flag - if user is interested and thinks of attending")

    event_type = relationship("EventType", backref="Event")
    website = relationship("Website", backref="Event")

    __table_args__ = (UniqueConstraint('name', 'description', 'website_id', name='unique_event'),)

    def update(self, source_event: "Event"):
        self.description = source_event.description
        self.location = source_event.location
        self.date_start = source_event.date_start
        self.date_end = source_event.date_end
        self.time_start = source_event.time_start
        self.time_end = source_event.time_end
        self.price = source_event.price
        self.tickets_left = source_event.tickets_left
        self.datetime_until = source_event.datetime_until
        self.url = source_event.url

    @staticmethod
    def create_event(structure: dict, event_type_id: int, website_id: int):
        ev = Event()
        ev.name = structure.get(EventEnums.NAME)
        ev.description = structure.get(EventEnums.DESCRIPTION)
        ev.location = structure.get(EventEnums.LOCATION)
        ev.date_start = structure.get(EventEnums.DATE_START)
        ev.date_end = structure.get(EventEnums.DATE_END)
        ev.time_start = structure.get(EventEnums.TIME_START)
        ev.time_end = structure.get(EventEnums.TIME_END)
        ev.price = structure.get(EventEnums.PRICE)
        ev.tickets_left = structure.get(EventEnums.TICKETS_LEFT)
        ev.datetime_until = structure.get(EventEnums.DATETIME_UNTIL)
        ev.url = structure.get(EventEnums.URL)
        ev.event_type_id = event_type_id
        ev.website_id = website_id
        return ev


engine = MySession.get_engine()
Base.metadata.create_all(engine)
engine.dispose()


class EventEnums:

    NAME: Final = 'name'
    DESCRIPTION: Final = 'description'
    LOCATION: Final = 'location'
    DATE_START: Final = 'date_start'
    DATE_END: Final = 'date_end'
    TIME_START: Final = 'time_start'
    TIME_END: Final = 'time_end'
    PRICE: Final = 'price'
    TICKETS_LEFT: Final = 'tickets_left'
    DATETIME_UNTIL: Final = 'datetime_until'
    URL: Final = 'url'
