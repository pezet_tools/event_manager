import psycopg2
from typing import Optional


class DBExecutor:

    db_executor: Optional["DBExecutor"]

    def __init__(self):
        ...

    @staticmethod
    def get_instance():
        if not DBExecutor.db_executor:
            DBExecutor.db_executor = DBExecutor()
        return DBExecutor.db_executor
