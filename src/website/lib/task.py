from ..scrappers.scrapper import Scrapper
from ..lib.session import MySession
from ..lib.models import Event


class Task:

    scrappers: dict[Scrapper.__name__: Scrapper] = dict()

    def __init__(self, scrapper: Scrapper, event_type_id: int, website_id: int):
        self.scrapper = scrapper
        self.event_type_id = event_type_id
        self.website_id = website_id
        self.session = MySession.get_session()

    def save_events(self, results: list[dict]):
        db_events = self.session.query(Event).filter_by(website_id=self.website_id).all()
        for result in results:
            event = Event.create_event(result, self.event_type_id, self.website_id)
            is_found = False
            for db_event in db_events:
                if event.name == db_event.name and event.description == db_event.description:
                    db_event.update(source_event=event)
                    is_found = True
                    break

            if not is_found:
                self.session.add(event)

        self.session.commit()

    def run_task(self):
        if isinstance(self.scrapper, Scrapper):
            __class_name = type(self.scrapper).__name__
            if Task.scrappers.get(__class_name):
                raise TaskAlreadyRunningError
            else:
                try:
                    Task.scrappers[__class_name] = self.scrapper
                    # run scrapper
                    results = self.scrapper.scrap_website()
                    # create db event and update db
                    self.save_events(results)
                finally:
                    del Task.scrappers[__class_name]
                return results
        else:
            raise ScrapperTypeNotImplemented


class TaskAlreadyRunningError(Exception):
    pass


class ScrapperTypeNotImplemented(Exception):
    pass


class ObjectNotAScrapper(Exception):
    pass
