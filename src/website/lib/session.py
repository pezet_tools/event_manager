from typing import Optional
from sqlalchemy import create_engine
from sqlalchemy.engine import URL
from sqlalchemy.orm import sessionmaker
from ..settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD


class MySession:

    my_session: Optional["MySession"] = None

    def __init__(self):
        self.url = URL.create(
            drivername="postgresql",
            username=DB_USER,
            password=DB_PASSWORD,
            host=DB_HOST,
            database=DB_USER,
            port=DB_PORT
        )

        self.engine = create_engine(self.url)
        session_class = sessionmaker(bind=self.engine)
        self.session = session_class()

    @staticmethod
    def get_instance():
        if not MySession.my_session:
            MySession.my_session = MySession()
        return MySession.my_session

    @staticmethod
    def get_session():
        return MySession().session

    @staticmethod
    def get_engine():
        return MySession().engine
