from .models import EventType, Website
from .session import MySession


def initial_event_type_data(session):
    default_data = [
        {"name": "Degustacje",
         "description": "Wydarzenia dotyczące degustacji alkoholi"},
        {"name": "Starty rakiet",
         "description": "Wydarzenia dotyczące startów rakiet "
                        "i innych obiektów udających się na orbitę ziemską oraz poza nią"},
        {"name": "Komunikacja miejska",
         "description": "Zmiany w komunikacji miejskiej w Warszawie"}
    ]

    event_types = session.query(EventType).all()
    if not event_types:
        for data in default_data:
            session.add(EventType(**data))
        session.commit()


def initial_website_data(session):
    default_data = [
        {"name": "M&P Alkohole",
         "url": "https://wina-mp.pl/kup-bilet",
         "description": "Strona dotycząca alkoholi"},
        {"name": "Starty rakiet",
         "url": "",
         "description": "Wydarzenia dotyczące startów rakiet "
                        "i innych obiektów udających się na orbitę ziemską oraz poza nią"},
        {"name": "WTP Warszawa",
         "url": "https://www.wtp.waw.pl/feed/?post_type=change",
         "description": "Strona dotycząca zmian w komunikacji miejskiej w Warszawie"}
    ]

    websites = session.query(Website).all()
    if not websites:
        for data in default_data:
            session.add(Website(**data))
        session.commit()


def insert_initial_values():
    session = MySession.get_session()
    initial_event_type_data(session)
    initial_website_data(session)
    session.close()
