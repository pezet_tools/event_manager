from fastapi import FastAPI
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html, get_redoc_html
from .routers import main_routers, scrapper_routers
from .lib.initial_data import insert_initial_values


app = FastAPI(
    title='Event Manager',
    openapi_url="/event_manager/openapi.json",
    docs_url="/event_manager/docs",
    redoc_url="/event_manager/redoc",
)

app.include_router(main_routers.router)
app.include_router(scrapper_routers.router)
app.mount("/static", StaticFiles(directory="website/static"), name="static")
# favicon_path = "/static/img/event_manager.jpg"


# @app.get("/favicon.ico", include_in_schema=False)
# async def favicon():
#     return FileResponse(favicon_path)


@app.get("/event_manager/docs", include_in_schema=False)
async def get_documentation():
    return get_swagger_ui_html(openapi_url="/event_manager/openapi.json",
                               title="Event Manager API documentation")


@app.get("/event_manager/redoc", include_in_schema=False)
async def redoc_html():
    return get_redoc_html(
        openapi_url="/event_manager/openapi.json",
        title="Event Manager API documentation"
    )


insert_initial_values()
main_routers.init(app)
