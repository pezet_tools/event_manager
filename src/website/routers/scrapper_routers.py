from fastapi import APIRouter
from fastapi.responses import RedirectResponse
from ..scrappers.mp_scrapper import MPScrapper
from ..scrappers.wtp_scrapper import WTPScrapper
from ..lib.task import Task, TaskAlreadyRunningError, ScrapperTypeNotImplemented, ObjectNotAScrapper
from ..lib.session import MySession
from ..lib.models import EventType, Website


router = APIRouter(
    prefix="/event_manager/scrapper",
    tags=["Scrapper"],
    responses={200: {'response': 'response result'},
               404: {"description": "Not found"}},
)


@router.get("/mp")
def update_mp():
    session = MySession.get_session()
    event_type_id = session.query(EventType).filter_by(id=1).first().id
    website = session.query(Website).filter_by(id=1).first()
    sc = MPScrapper(website.url)
    t = Task(sc, event_type_id, website.id)
    session.close()
    try:
        t.run_task()
    except TaskAlreadyRunningError as e:
        return {'response': type(e).__name__}
    except ScrapperTypeNotImplemented as e:
        return {'response': type(e).__name__}
    except ObjectNotAScrapper as e:
        return {'response': type(e).__name__}
    return RedirectResponse('/event_manager')


@router.get("/wtp")
def update_wtp():
    session = MySession.get_session()
    event_type_id = session.query(EventType).filter_by(id=3).first().id
    website = session.query(Website).filter_by(id=3).first()
    sc = WTPScrapper(website.url)
    t = Task(sc, event_type_id, website.id)
    session.close()
    try:
        t.run_task()
    except TaskAlreadyRunningError as e:
        return {'response': type(e).__name__}
    except ScrapperTypeNotImplemented as e:
        return {'response': type(e).__name__}
    except ObjectNotAScrapper as e:
        return {'response': type(e).__name__}
    return RedirectResponse('/event_manager')
