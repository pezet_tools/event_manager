from fastapi import FastAPI, APIRouter
from nicegui import ui, app
from sqlalchemy.sql import text
from ..frontend import main
from ..lib.session import MySession


router = APIRouter(
    prefix="/event_manager",
    tags=["Main"],
    responses={200: {'response': 'web page'},
               404: {"description": "Not found"}},
)


@router.get("/status")
def page_status():
    """
    Event manager main page
    """
    return {"response": "Website is up!"}


def init(main_app: FastAPI) -> None:
    @ui.page('/event_manager')
    def main_page():
        main.main_page()

    app.add_static_files('/static', 'website/static')
    ui.run_with(main_app) # , favicon="/static/img/event_manager.jpg")


@router.get("/delete_old_events")
def delete_old_events():
    """
    Delete events older than 2 months
    """
    print("Deleting old events...")
    engine = MySession.get_engine()
    with engine.connect() as connection:
        result = connection.execute(text("DELETE FROM EVENT WHERE DATE_START < (CURRENT_DATE - INTERVAL '2' MONTH);"))
        connection.commit()
        print(f"{result.rowcount} rows deleted")
        return {"response": f"{result.rowcount} rows deleted"}
