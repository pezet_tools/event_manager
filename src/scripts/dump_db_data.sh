#!/bin/bash
dump="db_dump.sql"
dd=$(date "+%Y%m%d_%H%M%S")
backup="${dd}_${dump}"

echo "#### Starting database dump - postgres..."
pg_dump "postgres" > "${dump}"
mv "${dump}" "${backup}"
gzip "${backup}"
echo "#### Database downloaded into ${backup}"
