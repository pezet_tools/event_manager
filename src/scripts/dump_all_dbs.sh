#!/bin/bash
dump="db_dump_all.sql"
dd=$(date "+%Y%m%d_%H%M%S")
backup="${dd}_${dump}"

echo "#### Starting full database dump..."
pg_dumpall > "${dump}"
mv "${dump}" "${backup}"
gzip "${backup}"
echo "#### Full database contents downloaded into ${backup}"
